import React, { useEffect, useState, useRef, useCallback, useReducer, Reducer, useContext } from 'react';
import './chat-emulator.css';
import { TextPreview } from '../components/text-preview';
import { ImgPreview } from '../components/img-preview';
import { VideoPreview } from '../components/video-preview';
import { ButtonListPreview } from '../components/buttonlist-preview';
import { AudioPreview } from '../components/audio-preview';
import { BotEngineClient } from '../components/bot-engine-client';
import { keyframes } from '@emotion/core';
import styled from '@emotion/styled'
import { ClientMsgType } from '../utils/models';
import { logg } from '../utils/logg';
import { ChatConext, ICPState } from '../store/chat-context';
import { LoaderView } from '../components/loader-view';
import { BotFunctions } from '../utils/bot-functions';
import { BotProperties } from '../utils/models'
import { Conf } from '../../config'

const { slideInLeft, slideInRight } = require('react-animations');

const InMsgAnimation = keyframes`${slideInLeft}`;
const OutMsgAnimation = keyframes`${slideInRight}`;
const InMsgDiv = styled.div`
  margin: 10px 0;
  animation: 0.4s ${InMsgAnimation};
`;
const OutMsgDiv = styled.div`
  margin: 10px 0;
  animation: 0.4s ${OutMsgAnimation};
`;

interface CPProps {
    visible: boolean,
    forceRestart: any;
    forceClear: any;
    botProperties: BotProperties
}

export default function ChatEmulator(props: any) {

    const [, updateState] = useState();
    const forceUpdate = useCallback(() => updateState({}), []);
    const { cvState, cvDispatch }: { cvState: ICPState, cvDispatch: any } = useContext(ChatConext);
    const messagesEndRef = useRef(null);
    const messageListDiv = useRef(null);

    const botFunc = new BotFunctions(props.botProperties, cvState, cvDispatch, forceUpdate, messagesEndRef);

    useEffect(() => {
        // botKitClient();
        (props.visible) && botFunc.initBotEngineClient();
    }, [props.visible])

    useEffect(() => {
        if (props.forceRestart > 0)
            (botFunc.botClient.restartBot) && botFunc.botClient.restartBot();
    }, [props.forceRestart]);

    useEffect(() => {
        if (props.forceClear > 0) {
            cvDispatch((state: ICPState) => {
                state.messageList = [];
                state.isTypingEnabled = true;
                return state
            });
        }
        logg.log("useEffect.cvState.messageList", cvState.messageList)
    }, [props.forceClear]);


    const onEnterPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
            botFunc.send({
                clientMsgType: ClientMsgType.USERTEXT,
                text: cvState.clientMessage,
            }, true);
        }
    }

    const onTextChange = (e: any) => {
        const _value = e.target ? e.target.value : "";
        cvDispatch((state: ICPState) => { state.clientMessage = _value; return state });

    }


    return (
        <React.Fragment>
            <main className="msger-chat" ref={messageListDiv}>
                {/* <FilePreview isLeft={true} userName="Bot" time="12:48" fileIcon={<SearchOutlined />} message="File" /> */}
                {cvState.messageList && cvState.messageList.map((item: any, idx: number) => {
                    switch (item.serverMsgType) {
                        case "ButtonList":
                            return <InMsgDiv key={idx}>
                                <ButtonListPreview isLeft={item.isLeft} userName="Bot" time={item.hm} onButtonClick={(item: any) => botFunc.onButtonsItemClick(item, idx)} message={item.text} buttonList={item.quick_replies} />
                            </InMsgDiv>
                        case "Text":
                            return <InMsgDiv key={idx}>
                                <TextPreview isLeft={item.isLeft} userName="Bot" time={item.hm} message={item.text} />
                            </InMsgDiv>
                        case "Image":
                            return <InMsgDiv key={idx}>
                                <ImgPreview isLeft={item.isLeft} userName="Bot" time={item.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
                            </InMsgDiv>
                        case "Video":
                            return <InMsgDiv key={idx}>
                                <VideoPreview isLeft={item.isLeft} userName="Bot" time={item.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
                            </InMsgDiv>
                        case "Audio":
                            return <InMsgDiv key={idx}>
                                <AudioPreview isLeft={item.isLeft} userName="Bot" time={item.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
                            </InMsgDiv>
                        default:
                            return <OutMsgDiv key={idx}>
                                <TextPreview isLeft={false} userName="You" time={item.hm} message={item.text} />
                            </OutMsgDiv>
                    }
                })}

                <LoaderView show={cvState.isThinking} />
                <div className="message-list-scrollto" ref={messagesEndRef} />

            </main>
            <div className={`${cvState.isTypingEnabled ? "msger-inputarea" : "msger-inputarea disabled"}`} >
                <input type="text" className="msger-input" onKeyDown={onEnterPress} value={cvState.clientMessage} onChange={(e: any) => onTextChange(e)} placeholder="Enter your message..." />
                <button type="submit" className="msger-send-btn" onClick={() => botFunc.send({ clientMsgType: ClientMsgType.USERTEXT, text: cvState.clientMessage }, true)}>Send</button>
            </div>
        </React.Fragment>
    )


}


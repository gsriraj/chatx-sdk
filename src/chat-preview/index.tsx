import { ChatConextProvider } from "../store/chat-context";
import React from "react";
import ChatEmulator from "./chat-emulator";

export default function ChatPreview(props: any) {
    return (
        <ChatConextProvider>
            <ChatEmulator {...props} />
        </ChatConextProvider>
        );
}
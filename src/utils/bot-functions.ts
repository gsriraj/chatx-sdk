import { IMessage, ClientMsgType, IMsgPayload } from "../utils/models";
import { logg } from "../utils/logg";
import { Util } from "../utils/util";
import { BotEngineClient } from "../components/bot-engine-client";
import { ICPState } from "../store/chat-context";
import { Subject } from "rxjs/internal/Subject";
import { BotProperties } from '../utils/models'

const inMessageSound = require("../assets/sounds/in.mp3");
const outMessageSound = require("../assets/sounds/out.mp3");

export class BotFunctions {
  botClient: BotEngineClient = BotEngineClient.getInstance(this.botProps);

  constructor(
    private botProps: BotProperties,
    private cvState: ICPState,
    private cvDispatch: any,
    private forceUpdate: () => void,
    private messagesEndRef: React.MutableRefObject<null>
  ) { }

  initBotEngineClient = () => {
    const subscribe = (param: any) => {
      logg.log("botEngineClient.cvState.messageList", this.cvState.messageList);
      const { event, message }: { event: string; message: IMessage } = param;
      switch (event) {
        case "message":
          const messages = Util.isSet(() => message.msgPayload, []);
          messages.forEach((element: IMsgPayload[], index: number) => {
            setTimeout(() => this.showMessages(element), 50 * (index + 1));
          });
          break;
        default:
          logg.log("Event not allowed!");
          break;
      }
    };
    this.botClient.addSubscription(subscribe);
    this.botClient.boot();
  };

  send = (message: IMessage, pushToMessageList: boolean): void => {
    this.cvDispatch((state: ICPState) => {
      state.isThinking = true;
      return state;
    });

    if (pushToMessageList) {
      const _messageList = this.cvState.messageList;
      message.hm = Util.hm(new Date());
      _messageList.push(message);
      this.cvDispatch((state: ICPState) => {
        state.messageList = _messageList;
        return state;
      });

      this.forceUpdate();
    }
    this.playIncomingMessageSound(outMessageSound);
    this.scrollToBottom();

    // socket$.next({JSON.stringify(message)});
    this.botClient.sendMessage(message);
    this.cvDispatch((state: ICPState) => {
      state.clientMessage = "";
      return state;
    });
    this.scrollToBottom();
  };

  onButtonsItemClick = (item: any, idx: number) => {
    this.cvDispatch((state: ICPState) => {
      state.isThinking = true;
      return state;
    });

    // Only last buttonlist is active
    if (
      this.cvState.messageList &&
      idx === this.cvState.messageList.length - 1
    ) {
      const _messageList = this.cvState.messageList;
      _messageList.push({
        text: item.title,
        hm: Util.hm(new Date()),
      });
      this.cvDispatch((state: ICPState) => {
        state.messageList = _messageList;
        return state;
      });
      this.forceUpdate();

      this.send(
        {
          clientMsgType: ClientMsgType.BUTTONS_SELECTION,
          text: item.payload,
        },
        false
      );
    }
  };

  playIncomingMessageSound = (msgSound: any) => {
    try {
      const audio = new Audio(msgSound);
      audio.play();
    } catch (e) {
      logg.log("playIncomingMessageSound:error", e);
    }
  };
  scrollToBottom = () => {
    setTimeout(() => {
      const mRef: any = Util.isSet(() => this.messagesEndRef.current, null);
      logg.log("mRef", mRef);
      mRef && mRef.scrollIntoView({ behavior: "smooth" });
    }, 1);
  };

  showMessages(message: IMessage) {
    const _messageList = this.cvState.messageList;
    logg.log("showMessages._messageList", _messageList);
    message.isLeft = true;
    message.hm = Util.hm(new Date());

    _messageList.push(message);
    this.cvDispatch((state: ICPState) => {
      state.messageList = _messageList;
      return state;
    });

    this.scrollToBottom();
    message.serverMsgType === "ButtonList"
      ? this.cvDispatch((state: ICPState) => {
        state.isTypingEnabled = false;
        return state;
      })
      : this.cvDispatch((state: ICPState) => {
        state.isTypingEnabled = true;
        return state;
      });

    this.forceUpdate();
    this.playIncomingMessageSound(inMessageSound);
    this.cvDispatch((state: ICPState) => {
      state.isThinking = false;
      return state;
    });
  }
}



export interface IChatPreviewProp {
    isLeft: boolean,
    userName: string,
    time: string,
    message?: string,
    url?: string,
    fileIcon?: any,
    buttonList?: Array<any>,
    onButtonClick?: any
}

export enum ClientMsgType {
  USERTEXT = 'usertext',
  BUTTONS_SELECTION = 'button_selection',
  REDIRECT = 'redirect',
  APICALL_REDIRECT = 'apicall_redirect'
}
export interface IMsgPayload{
    clientMsgType?: ClientMsgType;
    serverMsgType?: string;
    text?: string;
    quick_replies?: any;
  }
  export interface IMessage {
    isLeft?: boolean;
    type?: string;
    msgPayload?: IMsgPayload[];
    isBroadcast?: boolean;
    user?: string;
    jwt?: string | null | undefined;
    botId?: string;
    hm?:string;
    [key: string]: any;
  }

  export interface BotProperties {
    botID?: string;
    clientID?: string;
    appEngineHost?: string
  } 
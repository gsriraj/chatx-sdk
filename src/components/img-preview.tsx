import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';

export function ImgPreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-image-wrapper">
                    {(props.message) && <div className="msg-txt">
                        {props.message}
                    </div>}
                    <img className="msg-vid-img" src={props.url} />
                </div>
            </div>
        </div>
    )
}
import React from 'react';
import loaderIconUrl from '../assets/chat-loader.svg';
import { css, keyframes } from '@emotion/core'
import styled from '@emotion/styled'
const { slideInUp } = require('react-animations');
const InMsgAnimation = keyframes`${slideInUp}`;
const InMsgDiv = styled.div`
  margin: ${(props:any) => props.margin || '10px 0'};
  animation: 0.4s ${InMsgAnimation};
`;

const LoaderBubbleDiv = styled.div`
    border-radius: 15px 15px 15px 0;
    background: var(--left-msg-bg);
    max-width: fit-content;
`

const MsgWrapper = styled.div`
    width: 100%;
    display: flex;
`

export function LoaderView(props: any) {
    return (
        (props.show) ? <InMsgDiv {...props}>
            <MsgWrapper>
                <div className="left-msg cv-loader">
                    <LoaderBubbleDiv>
                        <img
                            style={{
                                width: "4em",
                                margin: "1em"
                            }}
                            src={loaderIconUrl} alt="" />
                    </LoaderBubbleDiv>
                </div>
            </MsgWrapper>
        </InMsgDiv> : <React.Fragment />
    )
}
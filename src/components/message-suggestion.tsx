import React, { useContext, useState, useEffect } from "react";
import { ICPState, ChatConext } from "../store/chat-context";
import Axios from "axios";
import { keyframes } from '@emotion/core';
import styled from '@emotion/styled'
import { logg } from "../utils/logg";
import store from "store"
const { fadeInUp } = require('react-animations');

const UpAnimation = keyframes`${fadeInUp}`;
const UpAnimationDiv = styled.div`
  animation: 0.4s ${UpAnimation};
`;

export default function MessageSuggesion(props: any) {
  const [suggesionList, setSuggesionList] = useState([]);
  useEffect(() => {
    if (props.userText.trim()) {
      Axios.get(`http://${props.botProps.appEngineHost}/suggestQnA/${store.get("current_bot_id")}?userText=${props.userText}`, {})
        .then((response) => {
          setSuggesionList(response.data);
        })
        .catch((error) => {
          logg.log("MessageSuggesion.get.error", error)
        });
    }
  }, [props.userText]);

  return (<div className="sc-suggession-wrapper" >
    {(props.shouldSuggest && suggesionList.length) ? <UpAnimationDiv>
      <div className="sc-suggessions" >
        {suggesionList.map((item: any, index: number) => {
          return <div key={index} className="sc-suggessions-text" onClick={()=> props.onSuggestion(item)} >{item}</div>
        })}
      </div>
    </UpAnimationDiv> : <React.Fragment />}
    {props.children}
  </div>)
};
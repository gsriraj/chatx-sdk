import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';

export function FilePreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-text">
                    {(props.message) && <div className="msg-txt">
                        {props.message}
                    </div>}

                    <div className="text-icon-wrapper" >
                        <div className="icon-wrapper" >
                            {props.fileIcon}
                        </div>
                        <div className="text-wrapper" >
                            {props.message}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
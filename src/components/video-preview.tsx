import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';

export function VideoPreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-vid-wrapper">
                    {(props.message) && <div className="msg-txt">
                        {props.message}
                    </div>}
                    <video className="msg-vid-img" src={props.url} controls >
                        <source src={props.url} type="video/mp4" />
                        This browser does not support HTML video.
                    </video>
                </div>
            </div>
        </div>
    )
}
import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';
import { List, Button } from 'antd';


export function ButtonListPreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-buttons">
                    {(props.message) && <div className="msg-txt">
                        {props.message}
                    </div>}

                    <List
                        className="msg-buttons-list"
                        bordered
                        dataSource={props.buttonList}
                        renderItem={(item: any) => (
                            <List.Item>
                                <Button type="link" onClick={() => props.onButtonClick(item)}>{item.title}</Button>
                            </List.Item>
                        )}
                    />
                </div>
            </div>
        </div>
    )
}
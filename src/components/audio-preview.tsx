import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';


export function AudioPreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-audio-wrapper">
                    {(props.message) && <div className="msg-txt">
                        {props.message}
                    </div>}
                    <audio className="msg-audio" src={props.url} controls >
                        <source src={props.url} type="audio/mpeg" />
                        This browser does not support HTML audio.
                    </audio>
                </div>
            </div>
        </div>
    )
}
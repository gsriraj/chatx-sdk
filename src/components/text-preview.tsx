import React, { useEffect, useState, useContext } from 'react';
import { IChatPreviewProp } from '../utils/models';
import ReactMarkdown from "react-markdown";

function Image(props:any) {
    return <img {...props} style={{maxWidth: '100%'}} />
  }

export function TextPreview(props: IChatPreviewProp) {
    return (
        <div className={`msg  ${(props.isLeft) ? "left-msg" : "right-msg"}`}>
            <div className="msg-bubble">
                <div className="msg-info">
                    <div className="msg-info-name">{props.userName}</div>
                    <div className="msg-info-time">{props.time}</div>
                </div>
                <div className="msg-text">
                    <ReactMarkdown source={props.message} renderers={{image: Image}} />
                </div>
            </div>
        </div>
    )
}
import store from "store";
import { IMessage, ClientMsgType } from '../utils/models';
import { logg } from "../utils/logg";
import { Util } from "../utils/util";
import { BotProperties } from '../utils/models'

export class BotEngineClient {
  private static instance: BotEngineClient;

  config: any = {
    http_url: (window.location.protocol === "https:" ? "https" : "https") +
      "://" + this.botProps.appEngineHost,
    ws_url:
      (window.location.protocol === "https:" ? "wss" : "ws") +
      "://" +
      this.botProps.appEngineHost,
    reconnect_timeout: 3000,
    max_reconnect: 5,
    enable_history: false,
  };
  options: any = {
    use_sockets: true,
  };
  reconnect_count: number = 0;
  guid: any = null;
  jwt: any = null;
  botId: any = null;
  current_user: any = null;
  socket: WebSocket = null as any;
  triggerMap: any = {};

  input: any;
  onEvent: Function;

  private constructor(private botProps: BotProperties) { }

  trigger(event: any, details: any) {
    this.triggerMap[event] && this.triggerMap[event](details);
  }

  request(url: string, body: any) {
    return new Promise(function (resolve, reject) {
      var xmlhttp = new XMLHttpRequest();

      xmlhttp.onreadystatechange = () => {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {
          if (xmlhttp.status === 200) {
            var response = xmlhttp.responseText;
            if (response !== "") {
              var message = null;
              try {
                message = JSON.parse(response);
              } catch (err) {
                reject(err);
                return;
              }
              resolve(message);
            } else {
              resolve([]);
            }
          } else {
            reject(new Error("status_" + xmlhttp.status));
          }
        }
      };

      xmlhttp.open("POST", url, true);
      xmlhttp.setRequestHeader("Content-Type", "application/json");
      xmlhttp.send(JSON.stringify(body));
    });
  }

  send(text: any, e: any = null) {
    if (e) e.preventDefault();
    if (!text) {
      return;
    }
    var message = {
      type: "outgoing",
      text: text,
    };

    this.clearReplies();
    this.renderMessage(message);

    this.deliverMessage({
      type: "message",
      text: text,
      user: this.guid,
      jwt: this.jwt,
      botId: this.botId,
      channel: this.options.use_sockets ? "websocket" : "webhook",
    });

    this.trigger("sent", message);

    return false;
  }

  restartBot() {
    this.deliverMessage({
      type: "welcome_back",
      user: this.guid,
      jwt: null,
      botId: this.botId,
      channel: "socket",
      user_profile: this.current_user ? this.current_user : null,
    });
  }

  sendMessage(message: any) {
    this.deliverMessage({
      ...message,
      type: "message",
      user: this.guid,
      jwt: this.jwt,
      botId: this.botId,
      channel: this.options.use_sockets ? "websocket" : "webhook",
    });
  }
  deliverMessage(message: IMessage) {
    if (this.options.use_sockets) {
      this.socket.send(JSON.stringify(message));
    } else {
      this.webhook(message);
    }
  }

  getHistory(guid: string = "") {
    if (this.guid) {
      this.request(`${this.config.http_url}/botkit/history`, {
        user: this.guid,
      })
        .then((history: any) => {
          if (history.success) {
            this.trigger("history_loaded", history.history);
          } else {
            this.trigger("history_error", new Error(history.error));
          }
        })
        .catch((err) => {
          this.trigger("history_error", err);
        });
    }
  }

  webhook(message: any) {
    this.request(`${this.config.http_url}/api/messages`, message)
      .then((messages: any) => {
        messages.forEach((message: any) => {
          this.trigger(message.type, message);
        });
      })
      .catch((err) => {
        this.trigger("webhook_error", err);
      });
  }

  connect(user: any) {
    if (user && user.id) {
      this.setCookie("bot_guid", user.id, 1);
      user.timezone_offset = new Date().getTimezoneOffset();
      this.current_user = user;
      logg.log("CONNECT WITH USER", user);
    }
    // connect to the chat server!
    if (this.options.use_sockets) {
      this.connectWebsocket(this.config.ws_url);
    } else {
      this.connectWebhook();
    }
  }

  connectWebhook() {
    var connectEvent = "hello";
    // this.botId = store.get("current_bot_id");
    // this.botId = store.get("botClientID");
    this.botId = this.botProps.botID
    if (this.getCookie("bot_guid")) {
      this.guid = this.getCookie("bot_guid");
      // this.botId = this.getCookie("bot_id");
      connectEvent = "welcome_back";
    } else {
      this.guid = this.generate_guid();
      this.setCookie("bot_guid", this.guid, 1);
      // this.setCookie('bot_id', this.botId, 1);
    }

    if (this.options.enable_history) {
      this.getHistory();
    }

    // connect immediately
    this.trigger("connected", {});
    this.webhook({
      type: connectEvent,
      user: this.guid,
      jwt: this.jwt,
      botId: this.botId,
      channel: "webhook",
    });
  }
  connectWebsocket(ws_url: string) {
    // Create WebSocket connection.
    this.socket = new WebSocket(ws_url);

    var connectEvent = "hello";
    // this.botId = store.get("current_bot_id");
    // this.botId = store.get("botClientID");
    this.botId = this.botProps.botID
    if (this.getCookie("bot_guid")) {
      this.guid = this.getCookie("bot_guid");
      connectEvent = "welcome_back";
    } else {
      this.guid = this.generate_guid();
      this.setCookie("bot_guid", this.guid, 1);
    }

    if (this.options.enable_history) {
      this.getHistory();
    }

    // Connection opened
    this.socket.addEventListener("open", (event: any) => {
      logg.log("CONNECTED TO SOCKET");
      this.reconnect_count = 0;
      this.trigger("connected", event);

      const jwt = this.getCookie("chat_jwt");

      // if jwt already has data it, continue from last node
      !jwt && this.deliverMessage({
        type: connectEvent,
        user: this.guid,
        jwt: this.jwt,
        botId: this.botId,
        channel: "socket",
        user_profile: this.current_user ? this.current_user : null,
      });

    });

    this.socket.addEventListener("error", (event: any) => {
      console.error("ERROR", event);
    });

    this.socket.addEventListener("close", (event: any) => {
      logg.log("SOCKET CLOSED!");
      this.trigger("disconnected", event);
      if (this.reconnect_count < this.config.max_reconnect) {
        setTimeout(() => {
          logg.log("RECONNECTING ATTEMPT ", ++this.reconnect_count);
          this.connectWebsocket(this.config.ws_url);
        }, this.config.reconnect_timeout);
      } else {
        logg.log("bot offline");
      }
    });

    // Listen for messages
    this.socket.addEventListener("message", (event: any) => {
      var message = null;
      try {
        message = JSON.parse(event.data);
      } catch (err) {
        this.trigger("socket_error", err);
        return;
      }

      this.trigger(message.type, message);
    });
  }
  closeWS() {
    if (this.socket) this.socket.close();
  }
  clearReplies() { }

  quickReply(payload: any) {
    this.send(payload);
  }
  focus() { }

  triggerScript(script: any, thread: any) {
    this.deliverMessage({
      type: "trigger",
      user: this.guid,
      jwt: this.jwt,
      botId: this.botId,
      channel: "socket",
      script: script,
      thread: thread,
    });
  }

  identifyUser(user: any) {
    user.timezone_offset = new Date().getTimezoneOffset();

    this.guid = user.id;
    this.setCookie("bot_guid", user.id, 1);

    this.current_user = user;

    this.deliverMessage({
      type: "identify",
      user: this.guid,
      jwt: this.jwt,
      botId: this.botId,
      channel: "socket",
      user_profile: user,
    });
  }

  receiveCommand(event: any) {
    switch (event.data.name) {
      case "trigger":
        // tell Botkit to trigger a specific script/thread
        logg.log("TRIGGER", event.data.script, event.data.thread);
        this.triggerScript(event.data.script, event.data.thread);
        break;
      case "identify":
        // link this account info to this user
        logg.log("IDENTIFY", event.data.user);
        this.identifyUser(event.data.user);
        break;
      case "connect":
        // link this account info to this user
        this.connect(event.data.user);
        break;
      default:
        logg.log("UNKNOWN COMMAND", event.data);
    }
  }

  setCookie(cname: any, cvalue: any, exdays: any) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname: any) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(";");
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }
  generate_guid() {
    const s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    };
    return (
      s4() +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      "-" +
      s4() +
      s4() +
      s4()
    );
  }

  renderMessage(message: any) {
    this.onEvent({ event: "message", message });
  }
  boot(user: any = null) {
    logg.log("Booting up");
    this.triggerMap["connected"] = (params: any) => {
      //   this.message_window.className = "connected";
    };

    this.triggerMap["disconnected"] = (params: any) => {
      //   this.message_window.className = "disconnected";
    };

    this.triggerMap["webhook_error"] = (err: any) => {
      alert("Error sending message!");
      console.error("Webhook Error", err);
    };

    this.triggerMap["typing"] = (params: any) => {
      this.clearReplies();
      this.renderMessage({
        isTyping: true,
      });
    };

    this.triggerMap["sent"] = (params: any) => {
      // do something after sending
    };

    this.triggerMap["message"] = (message: any) => {
      this.jwt = Util.isSet(() => message.attachment.jwt, "");
      this.setCookie("chat_jwt", this.jwt, 1);
      logg.log("this.triggerMap['message']", message);
      const isCallAPI = Util.isSet(() => message.msgPayload[0].serverMsgType, "") === "CallApi"

      if (Util.isSet(() => message.attachment.hasRedirect, false)) {
        // In case of always true redirect to server, so that server can process current node transition
        !isCallAPI &&
          this.deliverMessage({
            type: "message",
            text: "REDIRECT",
            clientMsgType: ClientMsgType.REDIRECT,
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: this.options.use_sockets ? "websocket" : "webhook",
          });

        // In case of api result redirect to server, so that server can process api result transition
        isCallAPI &&
          this.deliverMessage({
            type: "message",
            text: "APICALL_REDIRECT",
            clientMsgType: ClientMsgType.APICALL_REDIRECT,
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: this.options.use_sockets ? "websocket" : "webhook"
          });
      }

      if (!isCallAPI) {
        this.renderMessage(message);
      }
    };

    this.triggerMap["history_loaded"] = (history: any) => {
      if (history) {
        for (var m = 0; m < history.length; m++) {
          this.renderMessage({
            text: history[m].text,
            type:
              history[m].type === "message_received" ? "outgoing" : "incoming", // set appropriate CSS class
          });
        }
      }
    };

    logg.log("Messenger booted in stand-alone mode");
    // this is a stand-alone client. connect immediately.
    this.connect(user);

    return this;
  }

  public addSubscription(_onEvent: Function) {
    this.onEvent = _onEvent
  }

  public static getInstance(botProps: BotProperties): BotEngineClient {
    if (!BotEngineClient.instance) {
      BotEngineClient.instance = new BotEngineClient(botProps);
    }
    return BotEngineClient.instance;
  }
}

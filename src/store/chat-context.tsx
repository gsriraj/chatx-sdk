import React, { createContext, useReducer, Reducer } from "react";


export interface ICPState {
  messageList: Array<any>;
  clientMessage: string;
  isTypingEnabled: boolean;
  sendMessage?: any;
  isThinking:boolean,
  [key: string]: any;
}

export interface ICPAction {
  type: string;
  value?: any;
}

const cpInitialState = {
    messageList: [],
    clientMessage: "",
    isTypingEnabled: true,
    sendMessage: (message: any) => { },
    isThinking:false
  };


const cpReducer = (state: ICPState = cpInitialState, updateArg: any) => {
  if (updateArg.constructor === Function) {
    return { ...state, ...updateArg(state) };
  } else {
    return { ...state, ...updateArg };
  }
}

export const ChatConext = createContext<any>(cpInitialState);

export const ChatConextProvider = (props: any) => {
  const [cvState, cvDispatch] = useReducer<Reducer<ICPState, ICPAction>, ICPState>(cpReducer, cpInitialState, () =>  cpInitialState );
  return (
    <ChatConext.Provider value={{ cvState, cvDispatch }}>
      {props.children}
    </ChatConext.Provider>
  );
};
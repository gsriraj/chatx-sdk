import React, { Component } from 'react';
import chatIconUrl from './../../assets/chat-icon.svg';
import { keyframes } from '@emotion/core';
import styled from '@emotion/styled'
import { ButtonListPreview } from '../../components/buttonlist-preview';
import { TextPreview } from '../../components/text-preview';
import { ImgPreview } from '../../components/img-preview';
import { VideoPreview } from '../../components/video-preview';
import { AudioPreview } from '../../components/audio-preview';
import { Util } from '../../utils/util';
import { BotProperties } from '../../utils/models'
import { Conf } from '../../../config'

const { slideInLeft, slideInRight } = require('react-animations');

const InMsgAnimation = keyframes`${slideInLeft}`;
const OutMsgAnimation = keyframes`${slideInRight}`;
const InMsgDiv = styled.div`
  margin: 10px 0;
  animation: 0.4s ${InMsgAnimation};
`;
const OutMsgDiv = styled.div`
  margin: 10px 0;
  animation: 0.4s ${OutMsgAnimation};
`;

export interface IMessageView {
  message: any;
  idx: number;
  onButtonsItemClick: any;
  botProps: BotProperties
}
function Message(props: IMessageView): any {

  const _renderMessageOfType = (type: any) => {
    switch (type) {

      case "ButtonList":
        return <InMsgDiv key={props.idx}>
          <ButtonListPreview isLeft={props.message.isLeft} userName="Bot" time={props.message.hm} onButtonClick={(item: any) => props.onButtonsItemClick(item, props.idx)} message={props.message.text} buttonList={props.message.quick_replies} />
        </InMsgDiv>
      case "Text":
        return <InMsgDiv key={props.idx}>
          <TextPreview isLeft={props.message.isLeft} userName="Bot" time={props.message.hm} message={props.message.text} />
        </InMsgDiv>
      case "Image":
        return <InMsgDiv key={props.idx}>
          <ImgPreview isLeft={props.message.isLeft} userName="Bot" time={props.message.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
        </InMsgDiv>
      case "Video":
        return <InMsgDiv key={props.idx}>
          <VideoPreview isLeft={props.message.isLeft} userName="Bot" time={props.message.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
        </InMsgDiv>
      case "Audio":
        return <InMsgDiv key={props.idx}>
          <AudioPreview isLeft={props.message.isLeft} userName="Bot" time={props.message.hm} url={`${Conf.coreApiHostStatic}static/${props.message.text}`} />
        </InMsgDiv>
      default:
        return <OutMsgDiv key={props.idx}>
          <TextPreview isLeft={false} userName="You" time={props.message.hm} message={props.message.text} />
        </OutMsgDiv>
    }
  }

  let contentClassList = [
    'sc-message--content',
    (props.message.isLeft ? 'received' : 'sent')
  ];

  return (
    <div className="sc-message">
      <div className={contentClassList.join(' ')}>
        <div className="sc-message--avatar" style={{
          backgroundImage: `url(${chatIconUrl})`
        }}></div>
        {_renderMessageOfType(props.message.serverMsgType)}
      </div>
    </div>);

}

export default Message;

import React, { Component } from 'react';
import closeIcon from '../assets/close.svg';
import clearIcon from '../assets/trash.svg';
import restartIcon from '../assets/refresh.svg';


function Header(props: any) {
  return (
    <div className="sc-header">
      <img className="sc-header--img" src={props.imageUrl} alt="" />
      <div className="sc-header--team-name"> {props.teamName} </div>
      <div className="sc-header--close-button" onClick={props.onClear}>
        <img src={clearIcon} alt="" title="Clear" />
      </div>
      <div className="sc-header--close-button" title="Restart"  onClick={props.onRestart}>
        <img src={restartIcon} alt="" />
      </div>
      <div className="sc-header--close-button" title="Close" onClick={props.onClose}>
        <img src={closeIcon} alt="" />
      </div>
    </div>
  );

}

export default Header;

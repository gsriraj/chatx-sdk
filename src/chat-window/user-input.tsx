import PropTypes from 'prop-types';
import React, { Component, useContext, useState, useReducer } from 'react';
import SendIcon from './icons/send-icon';
import FileIcon from './icons/file-icon';
import EmojiIcon from './icons/emoji-icon';
import PopupWindow from './popups/popup-window';
import EmojiPicker from './emoji-picker/emoji-picker';
import { ChatConext, ICPState } from '../store/chat-context';
import MessageSuggesion from '../components/message-suggestion';
import { logg } from '../utils/logg';
import { BotProperties } from '../utils/models'

export interface IUserInput {
  onSubmit: any;
  onFilesSelected: Function;
  onUserInput?: any;
  forceTextClear?:any;
  showEmoji: boolean;
  cvState?: ICPState,
  cvDispatch?: any;
  botProps: BotProperties
};
class UserInputImpl extends Component<IUserInput> {

  emojiPickerButton: any;
  _fileUploadButton: any;
  userInput: any;
  state: any;

  constructor(props: IUserInput) {
    super(props);
    this.state = {
      inputActive: false,
      inputHasText: false,
      emojiPickerIsOpen: false,
      emojiFilter: ''
    } as any;
  }

  componentDidMount() {
    this.emojiPickerButton = document.querySelector('#sc-emoji-picker-button');
  }

  componentDidUpdate(prevProps:any) {
    if (prevProps.forceTextClear !== this.props.forceTextClear && this.props.forceTextClear > 0) {
      this.userInput.innerHTML = '';
    }
  }

  handleKeyDown(event: any) {
    if (event.keyCode === 13 && !event.shiftKey) {
      return this._submitText(event);
    }
  }

  handleKeyUp(event: any) {
    const inputHasText = event.target.innerHTML.length !== 0 &&
      event.target.innerText !== '\n';
    this.setState({ inputHasText });
    this.props.onUserInput(event.target.innerText, inputHasText)
  }

  _showFilePicker() {
    this._fileUploadButton.click();
  }

  toggleEmojiPicker = (e: any) => {
    e.preventDefault();
    if (!this.state.emojiPickerIsOpen) {
      this.setState({ emojiPickerIsOpen: true });
    }
  }

  closeEmojiPicker = (e: any) => {
    if (this.emojiPickerButton.contains(e.target)) {
      e.stopPropagation();
      e.preventDefault();
    }
    this.setState({ emojiPickerIsOpen: false });
  }

  _submitText(event: any) {
    event.preventDefault();
    const text = this.userInput.textContent;
    if (text && text.length > 0) {
      this.props.onSubmit({
        author: 'me',
        type: 'text',
        data: { text }
      });
      this.userInput.innerHTML = '';
    }
  }

  _onFilesSelected(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.props.onFilesSelected(event.target.files);
    }
  }

  _handleEmojiPicked = (emoji: any) => {
    this.setState({ emojiPickerIsOpen: false });
    if (this.state.inputHasText) {
      this.userInput.innerHTML += emoji;
    } else {
      this.props.onSubmit({
        author: 'me',
        type: 'emoji',
        data: { emoji }
      });
    }
  }

  handleEmojiFilterChange = (event: any) => {
    const emojiFilter = event.target.value;
    this.setState({ emojiFilter });
  }

  _renderEmojiPopup = () => (
    <PopupWindow
      isOpen={this.state.emojiPickerIsOpen}
      onClickedOutside={this.closeEmojiPicker}
      onInputChange={this.handleEmojiFilterChange}
    >
      <EmojiPicker
        onEmojiPicked={this._handleEmojiPicked}
        filter={this.state.emojiFilter}
      />
    </PopupWindow>
  )

  _renderSendOrFileIcon() {
    if (this.state.inputHasText) {
      return (
        <div className="sc-user-input--button">
          <SendIcon onClick={this._submitText.bind(this)} />
        </div>
      );
    }
    return (
      <div className="sc-user-input--button">
        <FileIcon onClick={this._showFilePicker.bind(this)} />
        <input
          type="file"
          name="files[]"
          multiple
          ref={(e) => { this._fileUploadButton = e; }}
          onChange={this._onFilesSelected.bind(this)}
        />
      </div>
    );
  }

  render() {
    const { emojiPickerIsOpen, inputActive } = this.state as any;
    return (
      <form className={`${this.props.cvState?.isTypingEnabled ? "" : "disabled"} sc-user-input ${(inputActive ? 'active' : '')}`}>
        <div
          role="button"
          onFocus={() => { this.setState({ inputActive: true }); }}
          onBlur={() => { this.setState({ inputActive: false }); }}
          ref={(e: any) => { this.userInput = e; }}
          onKeyDown={this.handleKeyDown.bind(this)}
          onKeyUp={this.handleKeyUp.bind(this)}
          contentEditable="true"
          placeholder="Write a reply..."
          className="sc-user-input--text"
        >
        </div>
        <div className="sc-user-input--buttons">
          <div className="sc-user-input--button"></div>
          <div className="sc-user-input--button">
            {this.props.showEmoji && <EmojiIcon
              onClick={this.toggleEmojiPicker}
              isActive={emojiPickerIsOpen}
              tooltip={this._renderEmojiPopup()}
            />}
          </div>
          {this._renderSendOrFileIcon()}
        </div>
      </form>
    );
  }
}

export default function UserInput(props: IUserInput) {
  const { cvState, cvDispatch }: { cvState: ICPState, cvDispatch: any } = useContext(ChatConext);
  const [shouldSuggest, setShouldSuggest] = useState(false);
  const [userText, setUserText] = useState("");
  const [forceTextClear, doForceTextClear] = useReducer(x => x + 1, 0);

  const onSuggestion = (sText: string) => {
    logg.log("onSuggestion", sText);
    props.onSubmit({
      type: 'text',
      data: { text: sText }
    });
    setShouldSuggest(false);
    doForceTextClear();
  };

  const onUserInput = (_userText: string, _inputHasText: boolean) => {
    if (_userText && _userText.length >= 3) {
      setShouldSuggest(_inputHasText);
      _inputHasText && setUserText(_userText);
    }else{
      setShouldSuggest(false);
    }
  };

  return <MessageSuggesion botProps={props.botProps} shouldSuggest={shouldSuggest} userText={userText} onSuggestion={onSuggestion}>
    <UserInputImpl cvState={cvState} cvDispatch={cvDispatch} onUserInput={onUserInput} forceTextClear={forceTextClear}  {...props} />
  </MessageSuggesion>



};
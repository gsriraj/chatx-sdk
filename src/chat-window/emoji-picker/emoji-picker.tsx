import React from 'react';
import emojiData from './emojiData.json';
const EmojiConvertor = require('emoji-js');

const emojiConvertor = new EmojiConvertor();
emojiConvertor.init_env();

const EmojiPicker = ({ onEmojiPicked, filter }:{ onEmojiPicked:any, filter:any }) => (
  <div className="sc-emoji-picker">
    {emojiData.map((category:any) => {
      const filteredEmojis = category.emojis.filter(({ name }:{ name:any }) => name.includes(filter));
      return (
        <div className="sc-emoji-picker--category" key={category.name}>
          {
            filteredEmojis.length > 0 &&
          <div className="sc-emoji-picker--category-title">{category.name}</div>
          }
          {filteredEmojis.map(({ char, _name }:{ char:any, _name:any }) => {
            return (
              <span
                key={char}
                className="sc-emoji-picker--emoji"
                onClick={() => onEmojiPicked(char)}
              >
                {char}
              </span>
            );
          })}
        </div>
      );
    })}
  </div>
);

export default EmojiPicker;

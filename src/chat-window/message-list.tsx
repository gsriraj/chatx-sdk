import React, { useEffect, useState, useRef, useCallback, useContext } from 'react';
import { logg } from '../utils/logg';
import Message from './messages';
import { ChatConext, ICPState } from '../store/chat-context';
import { LoaderView } from '../components/loader-view';
import { BotFunctions } from '../utils/bot-functions';
import { BotProperties } from '../utils/models'

interface CPProps {
  visible: boolean,
  forceRestart: any;
  forceClear: any;
  messages: any;
  imageUrl: string;
  botProps: BotProperties
}


function MessageList(props: CPProps) {

  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);

  const { cvState, cvDispatch }: { cvState: ICPState, cvDispatch: any } = useContext(ChatConext);
  const messagesEndRef = useRef(null);
  const messageListDiv = useRef(null);


  const botFunc = new BotFunctions(props.botProps, cvState, cvDispatch, forceUpdate, messagesEndRef);

  useEffect(() => {
    // botKitClient();
    (props.visible) && botFunc.initBotEngineClient();
    cvDispatch((state: ICPState) => { state.sendMessage = botFunc.send; return state });

  }, [props.visible])

  useEffect(() => {
    if (props.forceRestart > 0) {
      (botFunc.botClient.restartBot) && botFunc.botClient.restartBot();
    }
  }, [props.forceRestart]);

  useEffect(() => {
    if (props.forceClear > 0) {
      cvDispatch((state: ICPState) => {
        state.messageList = [];
        state.isTypingEnabled = true;
        return state
      });
    }
    logg.log("useEffect.cvState.messageList", cvState.messageList)
  }, [props.forceClear]);

  return (
    <div className="sc-message-list" ref={messageListDiv}>
      {cvState.messageList.map((item: any, idx: number) => {
        return <Message botProps={props.botProps} message={item} idx={idx} key={idx} onButtonsItemClick={botFunc.onButtonsItemClick} />;
      })}
      <LoaderView show={cvState.isThinking} margin="10px 10px 10px 20px" />
      <div className="message-list-scrollto" ref={messagesEndRef} >
      </div>
    </div>);

}

export default MessageList;



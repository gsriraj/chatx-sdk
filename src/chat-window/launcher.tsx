import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ChatWindow from './chat-window';
import launcherIcon from '../assets/logo-no-bg.svg';
import launcherIconActive from '../assets/close-icon.png';
const incomingMessageSound = require('../assets/sounds/in.mp3');
import '../styles';
import { ChatConextProvider } from '../store/chat-context';
import store from "store";
import { BotProperties } from '../utils/models'
import { logg } from '../utils/logg';

export interface ILauncher {
  onMessageWasReceived: Function;
  onMessageWasSent: Function;
  isOpen: boolean;
  handleClick: Function;
  messageList: Array<any>;
  showEmoji: boolean;
  newMessagesCount: number;
  mute: any;
  onFilesSelected: any;
  agentProfile: any;
  botProperties: BotProperties
};


class Launcher extends Component<ILauncher> {

  state: any;
  constructor(props: ILauncher) {
    super(props);
    this.state = {
      launcherIcon,
      isOpen: false
    };
  }

  componentWillReceiveProps(nextProps: any) {
    if (this.props.mute) { return; }
    logg.log("In launcher", this.props.botProperties)
    // store.set("botProperties", this.props.botProperties)
    const nextMessage = nextProps.messageList[nextProps.messageList.length - 1];
    const isIncoming = (nextMessage || {}).author === 'them';
    const isNew = nextProps.messageList.length > this.props.messageList.length;
    if (isIncoming && isNew) {
      this.playIncomingMessageSound();
    }
  }

  playIncomingMessageSound() {
    var audio = new Audio(incomingMessageSound);
    audio.play();
  }

  handleClick = () => {
    if (this.props.handleClick !== undefined) {
      this.props.handleClick();
    } else {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  }
  render() {
    const isOpen = this.props.hasOwnProperty('isOpen') ? this.props.isOpen : this.state.isOpen;
    const classList = [
      'sc-launcher',
      (isOpen ? 'opened' : ''),
    ];
    return (
      <React.Fragment>
        <ChatConextProvider>
          <div id="sc-launcher">
            <div className={classList.join(' ')} onClick={this.handleClick}>
              <MessageCount count={this.props.newMessagesCount} isOpen={isOpen} />
              <img className={'sc-open-icon'} src={launcherIconActive} />
              <img className={'sc-closed-icon'} src={launcherIcon} />
            </div>
            <ChatWindow
              messageList={this.props.messageList}
              onUserInputSubmit={this.props.onMessageWasSent}
              onFilesSelected={this.props.onFilesSelected}
              agentProfile={this.props.agentProfile}
              isOpen={isOpen}
              onClose={this.handleClick}
              showEmoji={this.props.showEmoji}
              botProps={this.props.botProperties}
            />
          </div>
        </ChatConextProvider>
      </React.Fragment>
    );
  }
}

const MessageCount = (props: any) => {
  if (props.count === 0 || props.isOpen === true) { return null; }
  return (
    <div className={'sc-new-messages-count'}>
      {props.count}
    </div>
  );
};

export default Launcher;

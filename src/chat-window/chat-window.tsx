import PropTypes from 'prop-types';
import React, { Component, useContext, useReducer } from 'react';
import MessageList from './message-list';
import UserInput from './user-input';
import Header from './header';
import { ChatConextProvider, ChatConext, ICPState } from '../store/chat-context';
import { ClientMsgType } from '../utils/models';
import { logg } from '../utils/logg';
import { BotProperties } from '../utils/models'

export interface IChatWindow {
  agentProfile: any;
  isOpen: boolean;
  onClose: Function;
  onFilesSelected: Function;
  onUserInputSubmit: Function;
  showEmoji: boolean;
  messageList: Array<any>;
  botProps: BotProperties
};

function ChatWindow(props: IChatWindow) {
  const { cvState, cvDispatch }: { cvState: ICPState, cvDispatch: any } = useContext(ChatConext);
  const [forceClear, doForceClear] = useReducer(x => x + 1, 0);
  const [forceRestart, doForceRestart] = useReducer(x => x + 1, 0);

  logg.log("In chatwindow", props.botProps)
  const onUserInputSubmit = (message: any) => {
    logg.log("onUserInputSubmit", message)
    cvState.sendMessage({
      clientMsgType: ClientMsgType.USERTEXT,
      text: message.data.text,
    }, true);
    props.onUserInputSubmit(message);
  }

  const onClear = () => {
    doForceClear()
  }

  const onRestart = () => {
    doForceRestart()
  }

  const onFilesSelected = (filesList: any) => {
    props.onFilesSelected(filesList);
  }

  let messageList = props.messageList || [];
  let classList = [
    'sc-chat-window',
    (props.isOpen ? 'opened' : 'closed')
  ];
  return (
    <div className={classList.join(' ')}>
      <Header
        teamName={props.agentProfile.teamName}
        imageUrl={props.agentProfile.imageUrl}
        onClose={props.onClose}
        onClear={onClear}
        onRestart={onRestart}
      />
      <MessageList
        visible={props.isOpen}
        messages={messageList}
        forceRestart={forceRestart}
        forceClear={forceClear}
        imageUrl={props.agentProfile.imageUrl}
        botProps={props.botProps}
      />
      <UserInput
        onSubmit={onUserInputSubmit}
        onFilesSelected={onFilesSelected}
        showEmoji={props.showEmoji}
        botProps={props.botProps}
      />
    </div>
  );

}

export default ChatWindow;

import * as path from 'path'

const ENV_FILE = path.join(__dirname, '..', '..', '.env')
require('dotenv').config({
    path: ENV_FILE,
})

export const Conf = {
    coreApiHostStatic: 'localhost:8080/'
}

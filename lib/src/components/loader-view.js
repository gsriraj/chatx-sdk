"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var chat_loader_svg_1 = __importDefault(require("../assets/chat-loader.svg"));
var core_1 = require("@emotion/core");
var styled_1 = __importDefault(require("@emotion/styled"));
var slideInUp = require('react-animations').slideInUp;
var InMsgAnimation = core_1.keyframes(templateObject_1 || (templateObject_1 = __makeTemplateObject(["", ""], ["", ""])), slideInUp);
var InMsgDiv = styled_1.default.div(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n  margin: ", ";\n  animation: 0.4s ", ";\n"], ["\n  margin: ", ";\n  animation: 0.4s ", ";\n"])), function (props) { return props.margin || '10px 0'; }, InMsgAnimation);
var LoaderBubbleDiv = styled_1.default.div(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n    border-radius: 15px 15px 15px 0;\n    background: var(--left-msg-bg);\n    max-width: fit-content;\n"], ["\n    border-radius: 15px 15px 15px 0;\n    background: var(--left-msg-bg);\n    max-width: fit-content;\n"])));
var MsgWrapper = styled_1.default.div(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n    width: 100%;\n    display: flex;\n"], ["\n    width: 100%;\n    display: flex;\n"])));
function LoaderView(props) {
    return ((props.show) ? react_1.default.createElement(InMsgDiv, __assign({}, props),
        react_1.default.createElement(MsgWrapper, null,
            react_1.default.createElement("div", { className: "left-msg cv-loader" },
                react_1.default.createElement(LoaderBubbleDiv, null,
                    react_1.default.createElement("img", { style: {
                            width: "4em",
                            margin: "1em"
                        }, src: chat_loader_svg_1.default, alt: "" }))))) : react_1.default.createElement(react_1.default.Fragment, null));
}
exports.LoaderView = LoaderView;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4;
//# sourceMappingURL=loader-view.js.map
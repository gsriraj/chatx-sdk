"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_markdown_1 = __importDefault(require("react-markdown"));
function Image(props) {
    return react_1.default.createElement("img", __assign({}, props, { style: { maxWidth: '100%' } }));
}
function TextPreview(props) {
    return (react_1.default.createElement("div", { className: "msg  " + ((props.isLeft) ? "left-msg" : "right-msg") },
        react_1.default.createElement("div", { className: "msg-bubble" },
            react_1.default.createElement("div", { className: "msg-info" },
                react_1.default.createElement("div", { className: "msg-info-name" }, props.userName),
                react_1.default.createElement("div", { className: "msg-info-time" }, props.time)),
            react_1.default.createElement("div", { className: "msg-text" },
                react_1.default.createElement(react_markdown_1.default, { source: props.message, renderers: { image: Image } })))));
}
exports.TextPreview = TextPreview;
//# sourceMappingURL=text-preview.js.map
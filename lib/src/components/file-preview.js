"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
function FilePreview(props) {
    return (react_1.default.createElement("div", { className: "msg  " + ((props.isLeft) ? "left-msg" : "right-msg") },
        react_1.default.createElement("div", { className: "msg-bubble" },
            react_1.default.createElement("div", { className: "msg-info" },
                react_1.default.createElement("div", { className: "msg-info-name" }, props.userName),
                react_1.default.createElement("div", { className: "msg-info-time" }, props.time)),
            react_1.default.createElement("div", { className: "msg-text" },
                (props.message) && react_1.default.createElement("div", { className: "msg-txt" }, props.message),
                react_1.default.createElement("div", { className: "text-icon-wrapper" },
                    react_1.default.createElement("div", { className: "icon-wrapper" }, props.fileIcon),
                    react_1.default.createElement("div", { className: "text-wrapper" }, props.message))))));
}
exports.FilePreview = FilePreview;
//# sourceMappingURL=file-preview.js.map
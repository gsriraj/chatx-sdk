"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = require("../utils/models");
var logg_1 = require("../utils/logg");
var util_1 = require("../utils/util");
var BotEngineClient = (function () {
    function BotEngineClient(botProps) {
        this.botProps = botProps;
        this.config = {
            http_url: (window.location.protocol === "https:" ? "https" : "https") +
                "://" + this.botProps.appEngineHost,
            ws_url: (window.location.protocol === "https:" ? "wss" : "ws") +
                "://" +
                this.botProps.appEngineHost,
            reconnect_timeout: 3000,
            max_reconnect: 5,
            enable_history: false,
        };
        this.options = {
            use_sockets: true,
        };
        this.reconnect_count = 0;
        this.guid = null;
        this.jwt = null;
        this.botId = null;
        this.current_user = null;
        this.socket = null;
        this.triggerMap = {};
    }
    BotEngineClient.prototype.trigger = function (event, details) {
        this.triggerMap[event] && this.triggerMap[event](details);
    };
    BotEngineClient.prototype.request = function (url, body) {
        return new Promise(function (resolve, reject) {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === XMLHttpRequest.DONE) {
                    if (xmlhttp.status === 200) {
                        var response = xmlhttp.responseText;
                        if (response !== "") {
                            var message = null;
                            try {
                                message = JSON.parse(response);
                            }
                            catch (err) {
                                reject(err);
                                return;
                            }
                            resolve(message);
                        }
                        else {
                            resolve([]);
                        }
                    }
                    else {
                        reject(new Error("status_" + xmlhttp.status));
                    }
                }
            };
            xmlhttp.open("POST", url, true);
            xmlhttp.setRequestHeader("Content-Type", "application/json");
            xmlhttp.send(JSON.stringify(body));
        });
    };
    BotEngineClient.prototype.send = function (text, e) {
        if (e === void 0) { e = null; }
        if (e)
            e.preventDefault();
        if (!text) {
            return;
        }
        var message = {
            type: "outgoing",
            text: text,
        };
        this.clearReplies();
        this.renderMessage(message);
        this.deliverMessage({
            type: "message",
            text: text,
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: this.options.use_sockets ? "websocket" : "webhook",
        });
        this.trigger("sent", message);
        return false;
    };
    BotEngineClient.prototype.restartBot = function () {
        this.deliverMessage({
            type: "welcome_back",
            user: this.guid,
            jwt: null,
            botId: this.botId,
            channel: "socket",
            user_profile: this.current_user ? this.current_user : null,
        });
    };
    BotEngineClient.prototype.sendMessage = function (message) {
        this.deliverMessage(__assign(__assign({}, message), { type: "message", user: this.guid, jwt: this.jwt, botId: this.botId, channel: this.options.use_sockets ? "websocket" : "webhook" }));
    };
    BotEngineClient.prototype.deliverMessage = function (message) {
        if (this.options.use_sockets) {
            this.socket.send(JSON.stringify(message));
        }
        else {
            this.webhook(message);
        }
    };
    BotEngineClient.prototype.getHistory = function (guid) {
        var _this = this;
        if (guid === void 0) { guid = ""; }
        if (this.guid) {
            this.request(this.config.http_url + "/botkit/history", {
                user: this.guid,
            })
                .then(function (history) {
                if (history.success) {
                    _this.trigger("history_loaded", history.history);
                }
                else {
                    _this.trigger("history_error", new Error(history.error));
                }
            })
                .catch(function (err) {
                _this.trigger("history_error", err);
            });
        }
    };
    BotEngineClient.prototype.webhook = function (message) {
        var _this = this;
        this.request(this.config.http_url + "/api/messages", message)
            .then(function (messages) {
            messages.forEach(function (message) {
                _this.trigger(message.type, message);
            });
        })
            .catch(function (err) {
            _this.trigger("webhook_error", err);
        });
    };
    BotEngineClient.prototype.connect = function (user) {
        if (user && user.id) {
            this.setCookie("bot_guid", user.id, 1);
            user.timezone_offset = new Date().getTimezoneOffset();
            this.current_user = user;
            logg_1.logg.log("CONNECT WITH USER", user);
        }
        if (this.options.use_sockets) {
            this.connectWebsocket(this.config.ws_url);
        }
        else {
            this.connectWebhook();
        }
    };
    BotEngineClient.prototype.connectWebhook = function () {
        var connectEvent = "hello";
        this.botId = this.botProps.botID;
        if (this.getCookie("bot_guid")) {
            this.guid = this.getCookie("bot_guid");
            connectEvent = "welcome_back";
        }
        else {
            this.guid = this.generate_guid();
            this.setCookie("bot_guid", this.guid, 1);
        }
        if (this.options.enable_history) {
            this.getHistory();
        }
        this.trigger("connected", {});
        this.webhook({
            type: connectEvent,
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: "webhook",
        });
    };
    BotEngineClient.prototype.connectWebsocket = function (ws_url) {
        var _this = this;
        this.socket = new WebSocket(ws_url);
        var connectEvent = "hello";
        this.botId = this.botProps.botID;
        if (this.getCookie("bot_guid")) {
            this.guid = this.getCookie("bot_guid");
            connectEvent = "welcome_back";
        }
        else {
            this.guid = this.generate_guid();
            this.setCookie("bot_guid", this.guid, 1);
        }
        if (this.options.enable_history) {
            this.getHistory();
        }
        this.socket.addEventListener("open", function (event) {
            logg_1.logg.log("CONNECTED TO SOCKET");
            _this.reconnect_count = 0;
            _this.trigger("connected", event);
            var jwt = _this.getCookie("chat_jwt");
            !jwt && _this.deliverMessage({
                type: connectEvent,
                user: _this.guid,
                jwt: _this.jwt,
                botId: _this.botId,
                channel: "socket",
                user_profile: _this.current_user ? _this.current_user : null,
            });
        });
        this.socket.addEventListener("error", function (event) {
            console.error("ERROR", event);
        });
        this.socket.addEventListener("close", function (event) {
            logg_1.logg.log("SOCKET CLOSED!");
            _this.trigger("disconnected", event);
            if (_this.reconnect_count < _this.config.max_reconnect) {
                setTimeout(function () {
                    logg_1.logg.log("RECONNECTING ATTEMPT ", ++_this.reconnect_count);
                    _this.connectWebsocket(_this.config.ws_url);
                }, _this.config.reconnect_timeout);
            }
            else {
                logg_1.logg.log("bot offline");
            }
        });
        this.socket.addEventListener("message", function (event) {
            var message = null;
            try {
                message = JSON.parse(event.data);
            }
            catch (err) {
                _this.trigger("socket_error", err);
                return;
            }
            _this.trigger(message.type, message);
        });
    };
    BotEngineClient.prototype.closeWS = function () {
        if (this.socket)
            this.socket.close();
    };
    BotEngineClient.prototype.clearReplies = function () { };
    BotEngineClient.prototype.quickReply = function (payload) {
        this.send(payload);
    };
    BotEngineClient.prototype.focus = function () { };
    BotEngineClient.prototype.triggerScript = function (script, thread) {
        this.deliverMessage({
            type: "trigger",
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: "socket",
            script: script,
            thread: thread,
        });
    };
    BotEngineClient.prototype.identifyUser = function (user) {
        user.timezone_offset = new Date().getTimezoneOffset();
        this.guid = user.id;
        this.setCookie("bot_guid", user.id, 1);
        this.current_user = user;
        this.deliverMessage({
            type: "identify",
            user: this.guid,
            jwt: this.jwt,
            botId: this.botId,
            channel: "socket",
            user_profile: user,
        });
    };
    BotEngineClient.prototype.receiveCommand = function (event) {
        switch (event.data.name) {
            case "trigger":
                logg_1.logg.log("TRIGGER", event.data.script, event.data.thread);
                this.triggerScript(event.data.script, event.data.thread);
                break;
            case "identify":
                logg_1.logg.log("IDENTIFY", event.data.user);
                this.identifyUser(event.data.user);
                break;
            case "connect":
                this.connect(event.data.user);
                break;
            default:
                logg_1.logg.log("UNKNOWN COMMAND", event.data);
        }
    };
    BotEngineClient.prototype.setCookie = function (cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };
    BotEngineClient.prototype.getCookie = function (cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(";");
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === " ") {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };
    BotEngineClient.prototype.generate_guid = function () {
        var s4 = function () {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        };
        return (s4() +
            s4() +
            "-" +
            s4() +
            "-" +
            s4() +
            "-" +
            s4() +
            "-" +
            s4() +
            s4() +
            s4());
    };
    BotEngineClient.prototype.renderMessage = function (message) {
        this.onEvent({ event: "message", message: message });
    };
    BotEngineClient.prototype.boot = function (user) {
        var _this = this;
        if (user === void 0) { user = null; }
        logg_1.logg.log("Booting up");
        this.triggerMap["connected"] = function (params) {
        };
        this.triggerMap["disconnected"] = function (params) {
        };
        this.triggerMap["webhook_error"] = function (err) {
            alert("Error sending message!");
            console.error("Webhook Error", err);
        };
        this.triggerMap["typing"] = function (params) {
            _this.clearReplies();
            _this.renderMessage({
                isTyping: true,
            });
        };
        this.triggerMap["sent"] = function (params) {
        };
        this.triggerMap["message"] = function (message) {
            _this.jwt = util_1.Util.isSet(function () { return message.attachment.jwt; }, "");
            _this.setCookie("chat_jwt", _this.jwt, 1);
            logg_1.logg.log("this.triggerMap['message']", message);
            var isCallAPI = util_1.Util.isSet(function () { return message.msgPayload[0].serverMsgType; }, "") === "CallApi";
            if (util_1.Util.isSet(function () { return message.attachment.hasRedirect; }, false)) {
                !isCallAPI &&
                    _this.deliverMessage({
                        type: "message",
                        text: "REDIRECT",
                        clientMsgType: models_1.ClientMsgType.REDIRECT,
                        user: _this.guid,
                        jwt: _this.jwt,
                        botId: _this.botId,
                        channel: _this.options.use_sockets ? "websocket" : "webhook",
                    });
                isCallAPI &&
                    _this.deliverMessage({
                        type: "message",
                        text: "APICALL_REDIRECT",
                        clientMsgType: models_1.ClientMsgType.APICALL_REDIRECT,
                        user: _this.guid,
                        jwt: _this.jwt,
                        botId: _this.botId,
                        channel: _this.options.use_sockets ? "websocket" : "webhook"
                    });
            }
            if (!isCallAPI) {
                _this.renderMessage(message);
            }
        };
        this.triggerMap["history_loaded"] = function (history) {
            if (history) {
                for (var m = 0; m < history.length; m++) {
                    _this.renderMessage({
                        text: history[m].text,
                        type: history[m].type === "message_received" ? "outgoing" : "incoming",
                    });
                }
            }
        };
        logg_1.logg.log("Messenger booted in stand-alone mode");
        this.connect(user);
        return this;
    };
    BotEngineClient.prototype.addSubscription = function (_onEvent) {
        this.onEvent = _onEvent;
    };
    BotEngineClient.getInstance = function (botProps) {
        if (!BotEngineClient.instance) {
            BotEngineClient.instance = new BotEngineClient(botProps);
        }
        return BotEngineClient.instance;
    };
    return BotEngineClient;
}());
exports.BotEngineClient = BotEngineClient;
//# sourceMappingURL=bot-engine-client.js.map
"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var axios_1 = __importDefault(require("axios"));
var core_1 = require("@emotion/core");
var styled_1 = __importDefault(require("@emotion/styled"));
var logg_1 = require("../utils/logg");
var store_1 = __importDefault(require("store"));
var fadeInUp = require('react-animations').fadeInUp;
var UpAnimation = core_1.keyframes(templateObject_1 || (templateObject_1 = __makeTemplateObject(["", ""], ["", ""])), fadeInUp);
var UpAnimationDiv = styled_1.default.div(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n  animation: 0.4s ", ";\n"], ["\n  animation: 0.4s ", ";\n"])), UpAnimation);
function MessageSuggesion(props) {
    var _a = react_1.useState([]), suggesionList = _a[0], setSuggesionList = _a[1];
    react_1.useEffect(function () {
        if (props.userText.trim()) {
            axios_1.default.get("http://" + props.botProps.appEngineHost + "/suggestQnA/" + store_1.default.get("current_bot_id") + "?userText=" + props.userText, {})
                .then(function (response) {
                setSuggesionList(response.data);
            })
                .catch(function (error) {
                logg_1.logg.log("MessageSuggesion.get.error", error);
            });
        }
    }, [props.userText]);
    return (react_1.default.createElement("div", { className: "sc-suggession-wrapper" },
        (props.shouldSuggest && suggesionList.length) ? react_1.default.createElement(UpAnimationDiv, null,
            react_1.default.createElement("div", { className: "sc-suggessions" }, suggesionList.map(function (item, index) {
                return react_1.default.createElement("div", { key: index, className: "sc-suggessions-text", onClick: function () { return props.onSuggestion(item); } }, item);
            }))) : react_1.default.createElement(react_1.default.Fragment, null),
        props.children));
}
exports.default = MessageSuggesion;
;
var templateObject_1, templateObject_2;
//# sourceMappingURL=message-suggestion.js.map
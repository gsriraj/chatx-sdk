"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
function VideoPreview(props) {
    return (react_1.default.createElement("div", { className: "msg  " + ((props.isLeft) ? "left-msg" : "right-msg") },
        react_1.default.createElement("div", { className: "msg-bubble" },
            react_1.default.createElement("div", { className: "msg-info" },
                react_1.default.createElement("div", { className: "msg-info-name" }, props.userName),
                react_1.default.createElement("div", { className: "msg-info-time" }, props.time)),
            react_1.default.createElement("div", { className: "msg-vid-wrapper" },
                (props.message) && react_1.default.createElement("div", { className: "msg-txt" }, props.message),
                react_1.default.createElement("video", { className: "msg-vid-img", src: props.url, controls: true },
                    react_1.default.createElement("source", { src: props.url, type: "video/mp4" }),
                    "This browser does not support HTML video.")))));
}
exports.VideoPreview = VideoPreview;
//# sourceMappingURL=video-preview.js.map
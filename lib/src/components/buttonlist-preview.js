"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var antd_1 = require("antd");
function ButtonListPreview(props) {
    return (react_1.default.createElement("div", { className: "msg  " + ((props.isLeft) ? "left-msg" : "right-msg") },
        react_1.default.createElement("div", { className: "msg-bubble" },
            react_1.default.createElement("div", { className: "msg-info" },
                react_1.default.createElement("div", { className: "msg-info-name" }, props.userName),
                react_1.default.createElement("div", { className: "msg-info-time" }, props.time)),
            react_1.default.createElement("div", { className: "msg-buttons" },
                (props.message) && react_1.default.createElement("div", { className: "msg-txt" }, props.message),
                react_1.default.createElement(antd_1.List, { className: "msg-buttons-list", bordered: true, dataSource: props.buttonList, renderItem: function (item) { return (react_1.default.createElement(antd_1.List.Item, null,
                        react_1.default.createElement(antd_1.Button, { type: "link", onClick: function () { return props.onButtonClick(item); } }, item.title))); } })))));
}
exports.ButtonListPreview = ButtonListPreview;
//# sourceMappingURL=buttonlist-preview.js.map
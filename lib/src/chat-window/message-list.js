"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var logg_1 = require("../utils/logg");
var messages_1 = __importDefault(require("./messages"));
var chat_context_1 = require("../store/chat-context");
var loader_view_1 = require("../components/loader-view");
var bot_functions_1 = require("../utils/bot-functions");
function MessageList(props) {
    var _a = react_1.useState(), updateState = _a[1];
    var forceUpdate = react_1.useCallback(function () { return updateState({}); }, []);
    var _b = react_1.useContext(chat_context_1.ChatConext), cvState = _b.cvState, cvDispatch = _b.cvDispatch;
    var messagesEndRef = react_1.useRef(null);
    var messageListDiv = react_1.useRef(null);
    var botFunc = new bot_functions_1.BotFunctions(props.botProps, cvState, cvDispatch, forceUpdate, messagesEndRef);
    react_1.useEffect(function () {
        (props.visible) && botFunc.initBotEngineClient();
        cvDispatch(function (state) { state.sendMessage = botFunc.send; return state; });
    }, [props.visible]);
    react_1.useEffect(function () {
        if (props.forceRestart > 0) {
            (botFunc.botClient.restartBot) && botFunc.botClient.restartBot();
        }
    }, [props.forceRestart]);
    react_1.useEffect(function () {
        if (props.forceClear > 0) {
            cvDispatch(function (state) {
                state.messageList = [];
                state.isTypingEnabled = true;
                return state;
            });
        }
        logg_1.logg.log("useEffect.cvState.messageList", cvState.messageList);
    }, [props.forceClear]);
    return (react_1.default.createElement("div", { className: "sc-message-list", ref: messageListDiv },
        cvState.messageList.map(function (item, idx) {
            return react_1.default.createElement(messages_1.default, { botProps: props.botProps, message: item, idx: idx, key: idx, onButtonsItemClick: botFunc.onButtonsItemClick });
        }),
        react_1.default.createElement(loader_view_1.LoaderView, { show: cvState.isThinking, margin: "10px 10px 10px 20px" }),
        react_1.default.createElement("div", { className: "message-list-scrollto", ref: messagesEndRef })));
}
exports.default = MessageList;
//# sourceMappingURL=message-list.js.map
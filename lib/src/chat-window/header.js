"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var close_svg_1 = __importDefault(require("../assets/close.svg"));
var trash_svg_1 = __importDefault(require("../assets/trash.svg"));
var refresh_svg_1 = __importDefault(require("../assets/refresh.svg"));
function Header(props) {
    return (react_1.default.createElement("div", { className: "sc-header" },
        react_1.default.createElement("img", { className: "sc-header--img", src: props.imageUrl, alt: "" }),
        react_1.default.createElement("div", { className: "sc-header--team-name" },
            " ",
            props.teamName,
            " "),
        react_1.default.createElement("div", { className: "sc-header--close-button", onClick: props.onClear },
            react_1.default.createElement("img", { src: trash_svg_1.default, alt: "", title: "Clear" })),
        react_1.default.createElement("div", { className: "sc-header--close-button", title: "Restart", onClick: props.onRestart },
            react_1.default.createElement("img", { src: refresh_svg_1.default, alt: "" })),
        react_1.default.createElement("div", { className: "sc-header--close-button", title: "Close", onClick: props.onClose },
            react_1.default.createElement("img", { src: close_svg_1.default, alt: "" }))));
}
exports.default = Header;
//# sourceMappingURL=header.js.map
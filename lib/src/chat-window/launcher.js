"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var chat_window_1 = __importDefault(require("./chat-window"));
var logo_no_bg_svg_1 = __importDefault(require("../assets/logo-no-bg.svg"));
var close_icon_png_1 = __importDefault(require("../assets/close-icon.png"));
var incomingMessageSound = require('../assets/sounds/in.mp3');
require("../styles");
var chat_context_1 = require("../store/chat-context");
var logg_1 = require("../utils/logg");
;
var Launcher = (function (_super) {
    __extends(Launcher, _super);
    function Launcher(props) {
        var _this = _super.call(this, props) || this;
        _this.handleClick = function () {
            if (_this.props.handleClick !== undefined) {
                _this.props.handleClick();
            }
            else {
                _this.setState({
                    isOpen: !_this.state.isOpen,
                });
            }
        };
        _this.state = {
            launcherIcon: logo_no_bg_svg_1.default,
            isOpen: false
        };
        return _this;
    }
    Launcher.prototype.componentWillReceiveProps = function (nextProps) {
        if (this.props.mute) {
            return;
        }
        logg_1.logg.log("In launcher", this.props.botProperties);
        var nextMessage = nextProps.messageList[nextProps.messageList.length - 1];
        var isIncoming = (nextMessage || {}).author === 'them';
        var isNew = nextProps.messageList.length > this.props.messageList.length;
        if (isIncoming && isNew) {
            this.playIncomingMessageSound();
        }
    };
    Launcher.prototype.playIncomingMessageSound = function () {
        var audio = new Audio(incomingMessageSound);
        audio.play();
    };
    Launcher.prototype.render = function () {
        var isOpen = this.props.hasOwnProperty('isOpen') ? this.props.isOpen : this.state.isOpen;
        var classList = [
            'sc-launcher',
            (isOpen ? 'opened' : ''),
        ];
        return (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(chat_context_1.ChatConextProvider, null,
                react_1.default.createElement("div", { id: "sc-launcher" },
                    react_1.default.createElement("div", { className: classList.join(' '), onClick: this.handleClick },
                        react_1.default.createElement(MessageCount, { count: this.props.newMessagesCount, isOpen: isOpen }),
                        react_1.default.createElement("img", { className: 'sc-open-icon', src: close_icon_png_1.default }),
                        react_1.default.createElement("img", { className: 'sc-closed-icon', src: logo_no_bg_svg_1.default })),
                    react_1.default.createElement(chat_window_1.default, { messageList: this.props.messageList, onUserInputSubmit: this.props.onMessageWasSent, onFilesSelected: this.props.onFilesSelected, agentProfile: this.props.agentProfile, isOpen: isOpen, onClose: this.handleClick, showEmoji: this.props.showEmoji, botProps: this.props.botProperties })))));
    };
    return Launcher;
}(react_1.Component));
var MessageCount = function (props) {
    if (props.count === 0 || props.isOpen === true) {
        return null;
    }
    return (react_1.default.createElement("div", { className: 'sc-new-messages-count' }, props.count));
};
exports.default = Launcher;
//# sourceMappingURL=launcher.js.map
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var message_list_1 = __importDefault(require("./message-list"));
var user_input_1 = __importDefault(require("./user-input"));
var header_1 = __importDefault(require("./header"));
var chat_context_1 = require("../store/chat-context");
var models_1 = require("../utils/models");
var logg_1 = require("../utils/logg");
;
function ChatWindow(props) {
    var _a = react_1.useContext(chat_context_1.ChatConext), cvState = _a.cvState, cvDispatch = _a.cvDispatch;
    var _b = react_1.useReducer(function (x) { return x + 1; }, 0), forceClear = _b[0], doForceClear = _b[1];
    var _c = react_1.useReducer(function (x) { return x + 1; }, 0), forceRestart = _c[0], doForceRestart = _c[1];
    logg_1.logg.log("In chatwindow", props.botProps);
    var onUserInputSubmit = function (message) {
        logg_1.logg.log("onUserInputSubmit", message);
        cvState.sendMessage({
            clientMsgType: models_1.ClientMsgType.USERTEXT,
            text: message.data.text,
        }, true);
        props.onUserInputSubmit(message);
    };
    var onClear = function () {
        doForceClear();
    };
    var onRestart = function () {
        doForceRestart();
    };
    var onFilesSelected = function (filesList) {
        props.onFilesSelected(filesList);
    };
    var messageList = props.messageList || [];
    var classList = [
        'sc-chat-window',
        (props.isOpen ? 'opened' : 'closed')
    ];
    return (react_1.default.createElement("div", { className: classList.join(' ') },
        react_1.default.createElement(header_1.default, { teamName: props.agentProfile.teamName, imageUrl: props.agentProfile.imageUrl, onClose: props.onClose, onClear: onClear, onRestart: onRestart }),
        react_1.default.createElement(message_list_1.default, { visible: props.isOpen, messages: messageList, forceRestart: forceRestart, forceClear: forceClear, imageUrl: props.agentProfile.imageUrl, botProps: props.botProps }),
        react_1.default.createElement(user_input_1.default, { onSubmit: onUserInputSubmit, onFilesSelected: onFilesSelected, showEmoji: props.showEmoji, botProps: props.botProps })));
}
exports.default = ChatWindow;
//# sourceMappingURL=chat-window.js.map
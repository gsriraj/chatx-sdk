"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var emojiData_json_1 = __importDefault(require("./emojiData.json"));
var EmojiConvertor = require('emoji-js');
var emojiConvertor = new EmojiConvertor();
emojiConvertor.init_env();
var EmojiPicker = function (_a) {
    var onEmojiPicked = _a.onEmojiPicked, filter = _a.filter;
    return (react_1.default.createElement("div", { className: "sc-emoji-picker" }, emojiData_json_1.default.map(function (category) {
        var filteredEmojis = category.emojis.filter(function (_a) {
            var name = _a.name;
            return name.includes(filter);
        });
        return (react_1.default.createElement("div", { className: "sc-emoji-picker--category", key: category.name },
            filteredEmojis.length > 0 &&
                react_1.default.createElement("div", { className: "sc-emoji-picker--category-title" }, category.name),
            filteredEmojis.map(function (_a) {
                var char = _a.char, _name = _a._name;
                return (react_1.default.createElement("span", { key: char, className: "sc-emoji-picker--emoji", onClick: function () { return onEmojiPicked(char); } }, char));
            })));
    })));
};
exports.default = EmojiPicker;
//# sourceMappingURL=emoji-picker.js.map
"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var send_icon_1 = __importDefault(require("./icons/send-icon"));
var file_icon_1 = __importDefault(require("./icons/file-icon"));
var emoji_icon_1 = __importDefault(require("./icons/emoji-icon"));
var popup_window_1 = __importDefault(require("./popups/popup-window"));
var emoji_picker_1 = __importDefault(require("./emoji-picker/emoji-picker"));
var chat_context_1 = require("../store/chat-context");
var message_suggestion_1 = __importDefault(require("../components/message-suggestion"));
var logg_1 = require("../utils/logg");
;
var UserInputImpl = (function (_super) {
    __extends(UserInputImpl, _super);
    function UserInputImpl(props) {
        var _this = _super.call(this, props) || this;
        _this.toggleEmojiPicker = function (e) {
            e.preventDefault();
            if (!_this.state.emojiPickerIsOpen) {
                _this.setState({ emojiPickerIsOpen: true });
            }
        };
        _this.closeEmojiPicker = function (e) {
            if (_this.emojiPickerButton.contains(e.target)) {
                e.stopPropagation();
                e.preventDefault();
            }
            _this.setState({ emojiPickerIsOpen: false });
        };
        _this._handleEmojiPicked = function (emoji) {
            _this.setState({ emojiPickerIsOpen: false });
            if (_this.state.inputHasText) {
                _this.userInput.innerHTML += emoji;
            }
            else {
                _this.props.onSubmit({
                    author: 'me',
                    type: 'emoji',
                    data: { emoji: emoji }
                });
            }
        };
        _this.handleEmojiFilterChange = function (event) {
            var emojiFilter = event.target.value;
            _this.setState({ emojiFilter: emojiFilter });
        };
        _this._renderEmojiPopup = function () { return (react_1.default.createElement(popup_window_1.default, { isOpen: _this.state.emojiPickerIsOpen, onClickedOutside: _this.closeEmojiPicker, onInputChange: _this.handleEmojiFilterChange },
            react_1.default.createElement(emoji_picker_1.default, { onEmojiPicked: _this._handleEmojiPicked, filter: _this.state.emojiFilter }))); };
        _this.state = {
            inputActive: false,
            inputHasText: false,
            emojiPickerIsOpen: false,
            emojiFilter: ''
        };
        return _this;
    }
    UserInputImpl.prototype.componentDidMount = function () {
        this.emojiPickerButton = document.querySelector('#sc-emoji-picker-button');
    };
    UserInputImpl.prototype.componentDidUpdate = function (prevProps) {
        if (prevProps.forceTextClear !== this.props.forceTextClear && this.props.forceTextClear > 0) {
            this.userInput.innerHTML = '';
        }
    };
    UserInputImpl.prototype.handleKeyDown = function (event) {
        if (event.keyCode === 13 && !event.shiftKey) {
            return this._submitText(event);
        }
    };
    UserInputImpl.prototype.handleKeyUp = function (event) {
        var inputHasText = event.target.innerHTML.length !== 0 &&
            event.target.innerText !== '\n';
        this.setState({ inputHasText: inputHasText });
        this.props.onUserInput(event.target.innerText, inputHasText);
    };
    UserInputImpl.prototype._showFilePicker = function () {
        this._fileUploadButton.click();
    };
    UserInputImpl.prototype._submitText = function (event) {
        event.preventDefault();
        var text = this.userInput.textContent;
        if (text && text.length > 0) {
            this.props.onSubmit({
                author: 'me',
                type: 'text',
                data: { text: text }
            });
            this.userInput.innerHTML = '';
        }
    };
    UserInputImpl.prototype._onFilesSelected = function (event) {
        if (event.target.files && event.target.files.length > 0) {
            this.props.onFilesSelected(event.target.files);
        }
    };
    UserInputImpl.prototype._renderSendOrFileIcon = function () {
        var _this = this;
        if (this.state.inputHasText) {
            return (react_1.default.createElement("div", { className: "sc-user-input--button" },
                react_1.default.createElement(send_icon_1.default, { onClick: this._submitText.bind(this) })));
        }
        return (react_1.default.createElement("div", { className: "sc-user-input--button" },
            react_1.default.createElement(file_icon_1.default, { onClick: this._showFilePicker.bind(this) }),
            react_1.default.createElement("input", { type: "file", name: "files[]", multiple: true, ref: function (e) { _this._fileUploadButton = e; }, onChange: this._onFilesSelected.bind(this) })));
    };
    UserInputImpl.prototype.render = function () {
        var _this = this;
        var _a;
        var _b = this.state, emojiPickerIsOpen = _b.emojiPickerIsOpen, inputActive = _b.inputActive;
        return (react_1.default.createElement("form", { className: (((_a = this.props.cvState) === null || _a === void 0 ? void 0 : _a.isTypingEnabled) ? "" : "disabled") + " sc-user-input " + (inputActive ? 'active' : '') },
            react_1.default.createElement("div", { role: "button", onFocus: function () { _this.setState({ inputActive: true }); }, onBlur: function () { _this.setState({ inputActive: false }); }, ref: function (e) { _this.userInput = e; }, onKeyDown: this.handleKeyDown.bind(this), onKeyUp: this.handleKeyUp.bind(this), contentEditable: "true", placeholder: "Write a reply...", className: "sc-user-input--text" }),
            react_1.default.createElement("div", { className: "sc-user-input--buttons" },
                react_1.default.createElement("div", { className: "sc-user-input--button" }),
                react_1.default.createElement("div", { className: "sc-user-input--button" }, this.props.showEmoji && react_1.default.createElement(emoji_icon_1.default, { onClick: this.toggleEmojiPicker, isActive: emojiPickerIsOpen, tooltip: this._renderEmojiPopup() })),
                this._renderSendOrFileIcon())));
    };
    return UserInputImpl;
}(react_1.Component));
function UserInput(props) {
    var _a = react_1.useContext(chat_context_1.ChatConext), cvState = _a.cvState, cvDispatch = _a.cvDispatch;
    var _b = react_1.useState(false), shouldSuggest = _b[0], setShouldSuggest = _b[1];
    var _c = react_1.useState(""), userText = _c[0], setUserText = _c[1];
    var _d = react_1.useReducer(function (x) { return x + 1; }, 0), forceTextClear = _d[0], doForceTextClear = _d[1];
    var onSuggestion = function (sText) {
        logg_1.logg.log("onSuggestion", sText);
        props.onSubmit({
            type: 'text',
            data: { text: sText }
        });
        setShouldSuggest(false);
        doForceTextClear();
    };
    var onUserInput = function (_userText, _inputHasText) {
        if (_userText && _userText.length >= 3) {
            setShouldSuggest(_inputHasText);
            _inputHasText && setUserText(_userText);
        }
        else {
            setShouldSuggest(false);
        }
    };
    return react_1.default.createElement(message_suggestion_1.default, { botProps: props.botProps, shouldSuggest: shouldSuggest, userText: userText, onSuggestion: onSuggestion },
        react_1.default.createElement(UserInputImpl, __assign({ cvState: cvState, cvDispatch: cvDispatch, onUserInput: onUserInput, forceTextClear: forceTextClear }, props)));
}
exports.default = UserInput;
;
//# sourceMappingURL=user-input.js.map
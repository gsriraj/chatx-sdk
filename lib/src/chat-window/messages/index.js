"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var chat_icon_svg_1 = __importDefault(require("./../../assets/chat-icon.svg"));
var core_1 = require("@emotion/core");
var styled_1 = __importDefault(require("@emotion/styled"));
var buttonlist_preview_1 = require("../../components/buttonlist-preview");
var text_preview_1 = require("../../components/text-preview");
var img_preview_1 = require("../../components/img-preview");
var video_preview_1 = require("../../components/video-preview");
var audio_preview_1 = require("../../components/audio-preview");
var config_1 = require("../../../config");
var _a = require('react-animations'), slideInLeft = _a.slideInLeft, slideInRight = _a.slideInRight;
var InMsgAnimation = core_1.keyframes(templateObject_1 || (templateObject_1 = __makeTemplateObject(["", ""], ["", ""])), slideInLeft);
var OutMsgAnimation = core_1.keyframes(templateObject_2 || (templateObject_2 = __makeTemplateObject(["", ""], ["", ""])), slideInRight);
var InMsgDiv = styled_1.default.div(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"], ["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"])), InMsgAnimation);
var OutMsgDiv = styled_1.default.div(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"], ["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"])), OutMsgAnimation);
function Message(props) {
    var _renderMessageOfType = function (type) {
        switch (type) {
            case "ButtonList":
                return react_1.default.createElement(InMsgDiv, { key: props.idx },
                    react_1.default.createElement(buttonlist_preview_1.ButtonListPreview, { isLeft: props.message.isLeft, userName: "Bot", time: props.message.hm, onButtonClick: function (item) { return props.onButtonsItemClick(item, props.idx); }, message: props.message.text, buttonList: props.message.quick_replies }));
            case "Text":
                return react_1.default.createElement(InMsgDiv, { key: props.idx },
                    react_1.default.createElement(text_preview_1.TextPreview, { isLeft: props.message.isLeft, userName: "Bot", time: props.message.hm, message: props.message.text }));
            case "Image":
                return react_1.default.createElement(InMsgDiv, { key: props.idx },
                    react_1.default.createElement(img_preview_1.ImgPreview, { isLeft: props.message.isLeft, userName: "Bot", time: props.message.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
            case "Video":
                return react_1.default.createElement(InMsgDiv, { key: props.idx },
                    react_1.default.createElement(video_preview_1.VideoPreview, { isLeft: props.message.isLeft, userName: "Bot", time: props.message.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
            case "Audio":
                return react_1.default.createElement(InMsgDiv, { key: props.idx },
                    react_1.default.createElement(audio_preview_1.AudioPreview, { isLeft: props.message.isLeft, userName: "Bot", time: props.message.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
            default:
                return react_1.default.createElement(OutMsgDiv, { key: props.idx },
                    react_1.default.createElement(text_preview_1.TextPreview, { isLeft: false, userName: "You", time: props.message.hm, message: props.message.text }));
        }
    };
    var contentClassList = [
        'sc-message--content',
        (props.message.isLeft ? 'received' : 'sent')
    ];
    return (react_1.default.createElement("div", { className: "sc-message" },
        react_1.default.createElement("div", { className: contentClassList.join(' ') },
            react_1.default.createElement("div", { className: "sc-message--avatar", style: {
                    backgroundImage: "url(" + chat_icon_svg_1.default + ")"
                } }),
            _renderMessageOfType(props.message.serverMsgType))));
}
exports.default = Message;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4;
//# sourceMappingURL=index.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Util = (function () {
    function Util() {
    }
    Util.isSet = function (fn, dv) {
        try {
            if (fn()) {
                return fn();
            }
            else {
                return dv;
            }
        }
        catch (e) {
            return dv;
        }
    };
    Util.guidGenerator = function () {
        var S4 = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4());
    };
    Util.uidSmall = function () {
        var firstPart = (Math.random() * 46656) | 0;
        var secondPart = (Math.random() * 46656) | 0;
        firstPart = ("000" + firstPart.toString(36)).slice(-3);
        secondPart = ("000" + secondPart.toString(36)).slice(-3);
        return firstPart + secondPart;
    };
    Util.hm = function (date) {
        var d = new Date(date);
        var h = d.getHours();
        var m = d.getMinutes();
        return h + ':' + m;
    };
    return Util;
}());
exports.Util = Util;
//# sourceMappingURL=util.js.map
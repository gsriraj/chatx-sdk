"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = require("../utils/models");
var logg_1 = require("../utils/logg");
var util_1 = require("../utils/util");
var bot_engine_client_1 = require("../components/bot-engine-client");
var inMessageSound = require("../assets/sounds/in.mp3");
var outMessageSound = require("../assets/sounds/out.mp3");
var BotFunctions = (function () {
    function BotFunctions(botProps, cvState, cvDispatch, forceUpdate, messagesEndRef) {
        var _this = this;
        this.botProps = botProps;
        this.cvState = cvState;
        this.cvDispatch = cvDispatch;
        this.forceUpdate = forceUpdate;
        this.messagesEndRef = messagesEndRef;
        this.botClient = bot_engine_client_1.BotEngineClient.getInstance(this.botProps);
        this.initBotEngineClient = function () {
            var subscribe = function (param) {
                logg_1.logg.log("botEngineClient.cvState.messageList", _this.cvState.messageList);
                var event = param.event, message = param.message;
                switch (event) {
                    case "message":
                        var messages = util_1.Util.isSet(function () { return message.msgPayload; }, []);
                        messages.forEach(function (element, index) {
                            setTimeout(function () { return _this.showMessages(element); }, 50 * (index + 1));
                        });
                        break;
                    default:
                        logg_1.logg.log("Event not allowed!");
                        break;
                }
            };
            _this.botClient.addSubscription(subscribe);
            _this.botClient.boot();
        };
        this.send = function (message, pushToMessageList) {
            _this.cvDispatch(function (state) {
                state.isThinking = true;
                return state;
            });
            if (pushToMessageList) {
                var _messageList_1 = _this.cvState.messageList;
                message.hm = util_1.Util.hm(new Date());
                _messageList_1.push(message);
                _this.cvDispatch(function (state) {
                    state.messageList = _messageList_1;
                    return state;
                });
                _this.forceUpdate();
            }
            _this.playIncomingMessageSound(outMessageSound);
            _this.scrollToBottom();
            _this.botClient.sendMessage(message);
            _this.cvDispatch(function (state) {
                state.clientMessage = "";
                return state;
            });
            _this.scrollToBottom();
        };
        this.onButtonsItemClick = function (item, idx) {
            _this.cvDispatch(function (state) {
                state.isThinking = true;
                return state;
            });
            if (_this.cvState.messageList &&
                idx === _this.cvState.messageList.length - 1) {
                var _messageList_2 = _this.cvState.messageList;
                _messageList_2.push({
                    text: item.title,
                    hm: util_1.Util.hm(new Date()),
                });
                _this.cvDispatch(function (state) {
                    state.messageList = _messageList_2;
                    return state;
                });
                _this.forceUpdate();
                _this.send({
                    clientMsgType: models_1.ClientMsgType.BUTTONS_SELECTION,
                    text: item.payload,
                }, false);
            }
        };
        this.playIncomingMessageSound = function (msgSound) {
            try {
                var audio = new Audio(msgSound);
                audio.play();
            }
            catch (e) {
                logg_1.logg.log("playIncomingMessageSound:error", e);
            }
        };
        this.scrollToBottom = function () {
            setTimeout(function () {
                var mRef = util_1.Util.isSet(function () { return _this.messagesEndRef.current; }, null);
                logg_1.logg.log("mRef", mRef);
                mRef && mRef.scrollIntoView({ behavior: "smooth" });
            }, 1);
        };
    }
    BotFunctions.prototype.showMessages = function (message) {
        var _messageList = this.cvState.messageList;
        logg_1.logg.log("showMessages._messageList", _messageList);
        message.isLeft = true;
        message.hm = util_1.Util.hm(new Date());
        _messageList.push(message);
        this.cvDispatch(function (state) {
            state.messageList = _messageList;
            return state;
        });
        this.scrollToBottom();
        message.serverMsgType === "ButtonList"
            ? this.cvDispatch(function (state) {
                state.isTypingEnabled = false;
                return state;
            })
            : this.cvDispatch(function (state) {
                state.isTypingEnabled = true;
                return state;
            });
        this.forceUpdate();
        this.playIncomingMessageSound(inMessageSound);
        this.cvDispatch(function (state) {
            state.isThinking = false;
            return state;
        });
    };
    return BotFunctions;
}());
exports.BotFunctions = BotFunctions;
//# sourceMappingURL=bot-functions.js.map
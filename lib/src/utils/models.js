"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ClientMsgType;
(function (ClientMsgType) {
    ClientMsgType["USERTEXT"] = "usertext";
    ClientMsgType["BUTTONS_SELECTION"] = "button_selection";
    ClientMsgType["REDIRECT"] = "redirect";
    ClientMsgType["APICALL_REDIRECT"] = "apicall_redirect";
})(ClientMsgType = exports.ClientMsgType || (exports.ClientMsgType = {}));
//# sourceMappingURL=models.js.map
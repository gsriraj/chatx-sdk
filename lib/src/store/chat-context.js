"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var cpInitialState = {
    messageList: [],
    clientMessage: "",
    isTypingEnabled: true,
    sendMessage: function (message) { },
    isThinking: false
};
var cpReducer = function (state, updateArg) {
    if (state === void 0) { state = cpInitialState; }
    if (updateArg.constructor === Function) {
        return __assign(__assign({}, state), updateArg(state));
    }
    else {
        return __assign(__assign({}, state), updateArg);
    }
};
exports.ChatConext = react_1.createContext(cpInitialState);
exports.ChatConextProvider = function (props) {
    var _a = react_1.useReducer(cpReducer, cpInitialState, function () { return cpInitialState; }), cvState = _a[0], cvDispatch = _a[1];
    return (react_1.default.createElement(exports.ChatConext.Provider, { value: { cvState: cvState, cvDispatch: cvDispatch } }, props.children));
};
//# sourceMappingURL=chat-context.js.map
"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
require("./chat-emulator.css");
var text_preview_1 = require("../components/text-preview");
var img_preview_1 = require("../components/img-preview");
var video_preview_1 = require("../components/video-preview");
var buttonlist_preview_1 = require("../components/buttonlist-preview");
var audio_preview_1 = require("../components/audio-preview");
var core_1 = require("@emotion/core");
var styled_1 = __importDefault(require("@emotion/styled"));
var models_1 = require("../utils/models");
var logg_1 = require("../utils/logg");
var chat_context_1 = require("../store/chat-context");
var loader_view_1 = require("../components/loader-view");
var bot_functions_1 = require("../utils/bot-functions");
var config_1 = require("../../config");
var _a = require('react-animations'), slideInLeft = _a.slideInLeft, slideInRight = _a.slideInRight;
var InMsgAnimation = core_1.keyframes(templateObject_1 || (templateObject_1 = __makeTemplateObject(["", ""], ["", ""])), slideInLeft);
var OutMsgAnimation = core_1.keyframes(templateObject_2 || (templateObject_2 = __makeTemplateObject(["", ""], ["", ""])), slideInRight);
var InMsgDiv = styled_1.default.div(templateObject_3 || (templateObject_3 = __makeTemplateObject(["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"], ["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"])), InMsgAnimation);
var OutMsgDiv = styled_1.default.div(templateObject_4 || (templateObject_4 = __makeTemplateObject(["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"], ["\n  margin: 10px 0;\n  animation: 0.4s ", ";\n"])), OutMsgAnimation);
function ChatEmulator(props) {
    var _a = react_1.useState(), updateState = _a[1];
    var forceUpdate = react_1.useCallback(function () { return updateState({}); }, []);
    var _b = react_1.useContext(chat_context_1.ChatConext), cvState = _b.cvState, cvDispatch = _b.cvDispatch;
    var messagesEndRef = react_1.useRef(null);
    var messageListDiv = react_1.useRef(null);
    var botFunc = new bot_functions_1.BotFunctions(props.botProperties, cvState, cvDispatch, forceUpdate, messagesEndRef);
    react_1.useEffect(function () {
        (props.visible) && botFunc.initBotEngineClient();
    }, [props.visible]);
    react_1.useEffect(function () {
        if (props.forceRestart > 0)
            (botFunc.botClient.restartBot) && botFunc.botClient.restartBot();
    }, [props.forceRestart]);
    react_1.useEffect(function () {
        if (props.forceClear > 0) {
            cvDispatch(function (state) {
                state.messageList = [];
                state.isTypingEnabled = true;
                return state;
            });
        }
        logg_1.logg.log("useEffect.cvState.messageList", cvState.messageList);
    }, [props.forceClear]);
    var onEnterPress = function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            event.stopPropagation();
            botFunc.send({
                clientMsgType: models_1.ClientMsgType.USERTEXT,
                text: cvState.clientMessage,
            }, true);
        }
    };
    var onTextChange = function (e) {
        var _value = e.target ? e.target.value : "";
        cvDispatch(function (state) { state.clientMessage = _value; return state; });
    };
    return (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement("main", { className: "msger-chat", ref: messageListDiv },
            cvState.messageList && cvState.messageList.map(function (item, idx) {
                switch (item.serverMsgType) {
                    case "ButtonList":
                        return react_1.default.createElement(InMsgDiv, { key: idx },
                            react_1.default.createElement(buttonlist_preview_1.ButtonListPreview, { isLeft: item.isLeft, userName: "Bot", time: item.hm, onButtonClick: function (item) { return botFunc.onButtonsItemClick(item, idx); }, message: item.text, buttonList: item.quick_replies }));
                    case "Text":
                        return react_1.default.createElement(InMsgDiv, { key: idx },
                            react_1.default.createElement(text_preview_1.TextPreview, { isLeft: item.isLeft, userName: "Bot", time: item.hm, message: item.text }));
                    case "Image":
                        return react_1.default.createElement(InMsgDiv, { key: idx },
                            react_1.default.createElement(img_preview_1.ImgPreview, { isLeft: item.isLeft, userName: "Bot", time: item.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
                    case "Video":
                        return react_1.default.createElement(InMsgDiv, { key: idx },
                            react_1.default.createElement(video_preview_1.VideoPreview, { isLeft: item.isLeft, userName: "Bot", time: item.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
                    case "Audio":
                        return react_1.default.createElement(InMsgDiv, { key: idx },
                            react_1.default.createElement(audio_preview_1.AudioPreview, { isLeft: item.isLeft, userName: "Bot", time: item.hm, url: config_1.Conf.coreApiHostStatic + "static/" + props.message.text }));
                    default:
                        return react_1.default.createElement(OutMsgDiv, { key: idx },
                            react_1.default.createElement(text_preview_1.TextPreview, { isLeft: false, userName: "You", time: item.hm, message: item.text }));
                }
            }),
            react_1.default.createElement(loader_view_1.LoaderView, { show: cvState.isThinking }),
            react_1.default.createElement("div", { className: "message-list-scrollto", ref: messagesEndRef })),
        react_1.default.createElement("div", { className: "" + (cvState.isTypingEnabled ? "msger-inputarea" : "msger-inputarea disabled") },
            react_1.default.createElement("input", { type: "text", className: "msger-input", onKeyDown: onEnterPress, value: cvState.clientMessage, onChange: function (e) { return onTextChange(e); }, placeholder: "Enter your message..." }),
            react_1.default.createElement("button", { type: "submit", className: "msger-send-btn", onClick: function () { return botFunc.send({ clientMsgType: models_1.ClientMsgType.USERTEXT, text: cvState.clientMessage }, true); } }, "Send"))));
}
exports.default = ChatEmulator;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4;
//# sourceMappingURL=chat-emulator.js.map
"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
var path = __importStar(require("path"));
var ENV_FILE = path.join(__dirname, '..', '..', '.env');
require('dotenv').config({
    path: ENV_FILE,
});
exports.Conf = {
    coreApiHostStatic: 'localhost:8080/'
};
//# sourceMappingURL=config.js.map